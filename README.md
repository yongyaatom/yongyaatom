# portfolio

# Requirement 
flutter version: 3.3.8
flutterfire version: 0.2.7

# Use Fvm 
Install fvm using command in ubuntu
    - dart pub global activate fvm
    - export PATH="$PATH":"$HOME/.pub-cache/bin" (Inside .bashrc)
        and 
    - source .bashrc
    - fvm install 3.3.8 (In home directory of project where pubspec.yaml located)
    - fvm use 3.3.8 (Also add .fvm in .gitignore file)



