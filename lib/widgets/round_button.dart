import 'package:flutter/material.dart';

class RoundButton extends StatelessWidget {
  final bool? isIcon;
  final Color? color;

  final Function()? onTap;
  final IconData? icon;
  final Color? iconColor;

  final String? imagePath;

  const RoundButton({
    super.key,
    this.color = Colors.blue,
    this.onTap,
    this.icon,
    this.iconColor = Colors.white,
    this.isIcon = false,
    this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
        ),
        child: isIcon!
        ? Icon(
          icon,
          color: iconColor,
        )
        : Image.asset(
          imagePath!,
          height: 20.0,
          width: 20.0,
        ),
      )
    );
  }
}
