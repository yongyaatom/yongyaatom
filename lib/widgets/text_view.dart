import 'package:flutter/material.dart';

class TextView extends StatelessWidget {
  final String text;
  final Color? color;
  const TextView({
    super.key,
    required this.text,  
    this.color = Colors.black,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
      ),
    );
  }
}
