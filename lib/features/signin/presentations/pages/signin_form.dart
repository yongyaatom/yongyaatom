import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../../cores/constants/image_path.dart';
import '../../../../cores/helpers/router/component/app_routes.dart';
import '../../../../widgets/round_button.dart';
import '../../../../widgets/text_view.dart';

class SigninForm extends StatefulWidget {
  const SigninForm({super.key});

  @override
  State<SigninForm> createState() => _SigninFormState();
}

class _SigninFormState extends State<SigninForm> {
  String? _googleImagePath;
  String? _facebookImagePath; 

  @override
  void initState() {
    _googleImagePath = ImagePath.googleImagePath;
    _facebookImagePath = ImagePath.facebookImagePath;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final AppBar appBar = AppBar(
      title: InkWell(
        onTap: () => context.go(AppRoute.home.path),
        child: Row(
          children: const <Widget>[
            TextView(
              text: 'Portfolio',
              color: Colors.white,
            ),
          ],
        ),
      ),
    );

    final Row oauthSignin = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
          RoundButton(
          isIcon: false,
          color: Colors.white,
          imagePath: _googleImagePath,
          onTap: () => (){},
        ),
        const SizedBox(width: 20),

        RoundButton(
          isIcon: false,
          color: Colors.white,
          imagePath: _facebookImagePath,
          onTap: () => (){},
        ),
        const SizedBox(width: 20),

        RoundButton(
          isIcon: true,
          icon: Icons.phone,
          onTap: () => (){},
        ),
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height: 20.0),
          const TextView(text: 'or Sign in using'),
          const SizedBox(height: 30.0),
          oauthSignin,
          const SizedBox(height: 30.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const TextView(text: 'No account'),
              TextButton(
                onPressed: () => context.go(AppRoute.signup.path), 
                child: const TextView(
                  text: 'Signup',
                  color: Colors.lightBlue,
                )
              )
            ],
          ),
        ],
      ),
    );
  }
}
