part of 'signin_form_bloc.dart';

abstract class SigninFormEvent extends Equatable {
  const SigninFormEvent();

  @override
  List<Object> get props => <Object>[];
}
