part of 'signin_form_bloc.dart';

abstract class SigninFormState extends Equatable {
  const SigninFormState();
  
  @override
  List<Object> get props => <Object>[];
}

class SigninFormInitial extends SigninFormState {}
