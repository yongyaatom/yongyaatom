import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'signin_form_event.dart';
part 'signin_form_state.dart';

class SigninFormBloc extends Bloc<SigninFormEvent, SigninFormState> {
  SigninFormBloc() : super(SigninFormInitial()) {
    on<SigninFormEvent>((SigninFormEvent event, Emitter<SigninFormState> emit) {
    });
  }
}
