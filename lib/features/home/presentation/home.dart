import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../cores/dependency_injection/app_state.dart';
import '../../../cores/helpers/router/component/app_routes.dart';
import '../../../widgets/text_view.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final AppState appState = AppState.of(context);
    final bool isLoggedIn = appState.session.isLoggedIn;
    return Scaffold(
      drawer: const Drawer(),
      appBar: AppBar(
        title: const TextView(
          text: 'Portfolio',
          color: Colors.white,
        ),
        actions: <Widget>[
          isLoggedIn 
          ? Row(
            children: <Widget>[
              
              TextButton(
                onPressed: (){
                  log('Open Drawer');
                }, 
                child: const TextView(
                  text: 'Atom Yongya',
                  color: Colors.white,
                )
              ),
            ],
          )
          : TextButton(
              onPressed: (){
                context.go(AppRoute.login.path);
              }, 
              child: const TextView(
                text: 'Login',
                color: Colors.white,
              )
            ),
        ],
      ),
    );
  }
}
