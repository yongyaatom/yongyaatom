part of 'signup_form_bloc.dart';

abstract class SignupFormState extends Equatable {
  const SignupFormState();
  
  @override
  List<Object> get props => <Object>[];
}

class SignupFormInitial extends SignupFormState {}
