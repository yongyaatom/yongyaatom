import 'package:flutter/material.dart';

import '../../../widgets/text_view.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            TextView(
              text: 'No internet connection',
              color: Colors.black,
            )
          ],
        ),
      ),
    );
  }
}
