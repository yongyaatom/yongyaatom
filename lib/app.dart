import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cores/blocs/sessions/sessions_bloc.dart';
import 'cores/dependency_injection/app_state.dart';
import 'cores/reposotories/check_internet_connection.dart';
import 'features/no_internet/presentation/error_page.dart';

class App extends StatefulWidget {
  final AppState appState;
  const App({super.key, required this.appState});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void dispose() {
    final AppState appState = AppState.of(context);
    appState.disposeSubscription.disposeSubscription();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    const String title = 'MyPortfolio';
    return AppStateProvider(
      appState: widget.appState,
      child: MultiBlocProvider(
        providers: <BlocProvider<dynamic>>[
          BlocProvider<SessionsBloc>(create: (BuildContext context) => widget.appState.sessionsBloc),
          BlocProvider<CheckInternetConnection>(create: (BuildContext context) => widget.appState.checkInternetConnection)
        ],
        child: BlocBuilder<CheckInternetConnection, bool>(
          builder: (BuildContext context, bool state) {
            return MaterialApp.router(
              builder: (BuildContext context, Widget? child){
                return widget.appState.checkInternetConnection.isInternetConnected 
                ? child!
                : const ErrorPage();
              },
              theme: ThemeData(
                primarySwatch: Colors.blue,
                appBarTheme: AppBarTheme(
                  systemOverlayStyle: SystemUiOverlayStyle(
                    statusBarColor: Colors.white.withOpacity(0),
                  ),
                ),
              ),
              routeInformationParser: widget.appState.appRouters.appRoutes.routeInformationParser,
              routerDelegate: widget.appState.appRouters.appRoutes.routerDelegate,
              routeInformationProvider: widget.appState.appRouters.appRoutes.routeInformationProvider,
              title: title,
            );
          },
        ),
      ),
    );
  }
}
