import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';

import '../../app.dart';
import '../../cores/bootstrap/main_bootstrap.dart';
import '../../cores/dependency_injection/app_state.dart';
import '../../cores/enums/enum_collections.dart';
import '../../firebase_options.dart';

Future<void> mainCommon(Environment env) async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  
  final AppState appState = await mainBootstrap();
  
  switch(env){
    case Environment.prod:
      break;
    case Environment.staging:
      break;
    default:
      break;
  }

  runApp(App(
    appState: appState
  ));
}
