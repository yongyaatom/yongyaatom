import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import '../models/notification_model/notification_model.dart';
import '../models/session_token/session_token.dart';
import '../models/token_expire/token_expire_error.dart';
import '../models/users/users.dart';

part 'serialization.g.dart';

@SerializersFor(<Type>[
  TokenExpiredError,
  TokenExpiredErrorMessage,
  AccessToken,
  RefreshToken,
  UserModel,
  NotificationData,
])
final Serializers serializers = (
  _$serializers.toBuilder()
  ..addBuilderFactory(const FullType(BuiltList, <FullType>[FullType(TokenExpiredErrorMessage)]), () => ListBuilder<TokenExpiredErrorMessage>())
  ..addPlugin(StandardJsonPlugin())
).build();
