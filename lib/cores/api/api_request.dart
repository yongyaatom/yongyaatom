import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../enums/enum_collections.dart';
import '../exception/exceptions.dart';
import '../models/session_token/session_token.dart';
import '../models/token_expire/token_expire_error.dart';
import '../serialization/serialization.dart';
import 'components/api_request_typedef.dart';

class ApiRequest{
  final String host;
  final String scheme;
  final int port;
  final AccessTokenGetter getAccessToken;
  final RefreshTokenGetter getRefreshToken;
  final AccessTokenSetter setAccessToken;

  ApiRequest({
    required this.host,
    required String? initialAccessToken,
    required this.getAccessToken,
    required this.getRefreshToken,
    required this.setAccessToken,
    this.scheme = kDebugMode ? 'http' : 'https',
    this.port = kDebugMode ? 8000 : 80,
  }) : _accessToken = initialAccessToken , _accessTokenCompleter = Completer<String>(){
    _accessTokenCompleter.complete(initialAccessToken ?? '');
  } 

  late final String? _accessToken;
  late final Completer<String> _accessTokenCompleter;

  Future<T> apiRequest<T, D>({
    required String path,
    required HttpMethods httpMethods,
    D? data,
    bool isProtected = true,
    Map<String, String> additionalHeaders = const <String, String>{},
    VoidCallback? onSessionExpired,
    bool shouldRefreshAccessToken = true,
  }) async {
    late http.Response response;

    final Uri uri = Uri(
      path: path,
      port: port,
      scheme: scheme,
      host: host,
    );

    final Map<String, String> headers = <String, String>{
      'Content-Type': 'application/json',
    }..addAll(additionalHeaders);

    if (isProtected){
      headers.addAll(<String,String>{
        '': 'Bearer $_accessToken'
      });
    }

    try{
      final Object? body = serializers.serializeWith<Object?>(serializers.serializerForType(D)!, data);
      switch(httpMethods){
        case HttpMethods.get:
          response = await http.get(uri, headers: headers);
          break;
        case HttpMethods.post:
          response = await http.post(uri, headers: headers, body: jsonEncode(body));
          break;
        case HttpMethods.put:
          response = await http.put(uri, headers: headers, body: body);
          break;
        case HttpMethods.delete:
          response = await http.delete(uri, headers: headers);
          break;
      }
      return response as Future<T>;
    }catch(e){
      NetworkException();
    }

    if (response.statusCode == 200){
      final Object result = await serializers.deserializeWith<dynamic>(
        serializers.serializerForType(T)!, 
        jsonDecode(response.body)
      );
      return result as Future<T>;
    }

    if (response.statusCode == 401 && shouldRefreshAccessToken){
      final TokenExpiredError? errorResponse = serializers.deserializeWith(TokenExpiredError.serializer, json.decode(response.body));

      if ((errorResponse?.code ?? '') == 'token_not_valid'){
        if (errorResponse?.message.where((TokenExpiredErrorMessage m) => m.tokenType == 'access').isNotEmpty ?? false){
          await _refreshAccessToken(host);
          return await apiRequest(
            path: path, 
            httpMethods: httpMethods,
            data: data,
            isProtected: isProtected,
            additionalHeaders: additionalHeaders,
            shouldRefreshAccessToken: shouldRefreshAccessToken,
          );
        }

        throw SessionExpired(uri, 'Logged out');
      }
    }
    else if (response.statusCode == 400){
      throw BadRequest(uri, 'Invalid Request');
    }
    else if (response.statusCode == 500){
      throw ServerError(uri, 'Error in the server[500].');
    }
    else if (response.statusCode == 404){
      throw NotFound(uri, 'Content could not be found [404]');
    }

    throw UnKnownError(uri, 'Somethings went wrong: ${response.statusCode}');
  }

  Future<T> get<T>({
    required String path,
    bool isProtected = true,
    Map<String, String> additionalHeaders = const <String, String>{},
    VoidCallback? onSessionExpired,
    bool shouldRefreshAccessToken = true,
  }) async {
    return await apiRequest<T, dynamic>(
      path: path, 
      httpMethods: HttpMethods.get,
      isProtected: isProtected,
      additionalHeaders: additionalHeaders,
      onSessionExpired: onSessionExpired,
      shouldRefreshAccessToken: shouldRefreshAccessToken,
    );
  }

  Future<T> post <T, D>({
    required String path,
    bool isProtected = true,
    D? data,
    Map<String, String> additionalHeaders = const <String, String>{},
    VoidCallback? onSessionExpired,
    bool shouldRefreshAccessToken = true,
  }) async {
    return await apiRequest<T, D>(
      path: path, 
      httpMethods: HttpMethods.post,
      data: data,
      isProtected: isProtected,
      additionalHeaders: additionalHeaders,
      onSessionExpired: onSessionExpired,
      shouldRefreshAccessToken: shouldRefreshAccessToken,
    );
  }

  Future<T> put<T, D>({
    required String path,
    bool isProtected = true,
    D? data,
    Map<String, String> additionalHeaders = const <String, String> {},
    VoidCallback? onSessionExpired,
    bool shouldRefreshAccessToken = true,
  }) async {
    return await apiRequest<T, D>(
      path: path, 
      httpMethods: HttpMethods.put,
      data: data,
      additionalHeaders: additionalHeaders,
      onSessionExpired: onSessionExpired,
      shouldRefreshAccessToken: shouldRefreshAccessToken,
    );
  }

  Future<T> delete<T>({
    required String path,
    bool isProtected = true,
    Map<String, String> additionalHeaders = const <String, String>{},
    VoidCallback? onSessionExpired,
    bool shouldRefreshAccessToken = true,
  }) async {
    return await apiRequest<T, dynamic>(
      path: path, 
      httpMethods: HttpMethods.delete,
      isProtected: isProtected,
      additionalHeaders: additionalHeaders,
      onSessionExpired: onSessionExpired,
      shouldRefreshAccessToken: shouldRefreshAccessToken,
    );
  }

  Future<dynamic> _refreshAccessToken(String host) async {
    if (!_accessTokenCompleter.isCompleted){
      return _accessTokenCompleter.future;
    }

    try{
      _accessTokenCompleter = Completer<String>();
      final String? refreshToken = await getRefreshToken();
      final Object? body = serializers.serializeWith<Object?>(RefreshToken.serializer, RefreshToken((RefreshTokenBuilder b) => b..refreshToken = refreshToken));

      final http.Response refreshTokenResponse = await http.post(
        Uri(
          path: 'auth/token/refresh',
          host: host,
          scheme: scheme,
          port: port
        ),
        body: body,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'grant_type': 'refresh_token',
        }
      );

      if (refreshTokenResponse.statusCode != 200){
        _accessTokenCompleter.completeError(Exception('Request Error'));
      }

      final AccessToken? accessToken = serializers.deserializeWith(AccessToken.serializer, refreshTokenResponse.body);

      if (accessToken == null){
        _accessTokenCompleter.completeError(Exception('Failed to deserializer'));
        return;
      }

      _accessToken = accessToken.access;
      await setAccessToken(_accessToken!);
      _accessTokenCompleter.complete(_accessToken);
      return;
    }catch(e){
      _accessTokenCompleter.completeError(e);
      return;
    }
  }
}
