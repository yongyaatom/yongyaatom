import 'package:flutter/foundation.dart';

typedef CredentialSetter = Future<void> Function(String, String);
typedef CredentialDeleter = AsyncCallback;
typedef AccessTokenGetter = Future<String?>Function();
typedef RefreshTokenGetter = Future<String?>Function();
typedef AccessTokenSetter = Future<void>Function(String);
