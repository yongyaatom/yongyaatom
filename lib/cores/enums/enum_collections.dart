enum Environment{dev, staging, prod}

enum HttpMethods{get, post, put, delete}

enum UserState{
  guest, 
  standardUser,
  admin,
}

enum InternetConnection{
  wifiConnection,
  mobileDataConnection,
  noInternetConnection,
}
