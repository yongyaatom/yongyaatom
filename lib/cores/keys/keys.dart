class Keys{
  static const String accessToken = 'accessToken';
  static const String refreshToken = 'refreshToken';
  static const String foregroundMessage = 'foregroundMessage';
}
