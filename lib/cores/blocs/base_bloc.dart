
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../dependency_injection/app_state.dart';
import '../logger/logger.dart';

abstract class AppBloc<E, S> extends Bloc<E, S>{
  AppBloc(super.initialState, this.id);
  late final String id;

  @override
  String toString(){
    return '$runtimeType:\n\t$state';
  }
}

class AppBlocObserver implements BlocObserver{
  final AppState appState;
  final Logger logger;
  AppBlocObserver(
    this.appState,
    this.logger,
  );

  final List<AppBloc<dynamic, dynamic>> registredBlocs = <AppBloc<dynamic, dynamic>> [];

  @override
  void onChange(BlocBase<dynamic> bloc, Change<dynamic> change){}

  @override
  void onClose(BlocBase<dynamic> bloc) {
    if(bloc is AppBloc){
      final int blocIndex = registredBlocs.indexWhere((AppBloc<dynamic, dynamic> element) => element.id == bloc.id);
      debugPrint('Removed bloc: $bloc');
      registredBlocs.removeAt(blocIndex);
    }
  }

  @override
  void onCreate(BlocBase<dynamic> bloc) {
    if (bloc is AppBloc){
      final bool blocExists = registredBlocs.any((AppBloc<dynamic, dynamic> element) => element.id == element.id);

      if (blocExists){
        throw ArgumentError('Bloc with id: ${bloc.id} is already registred', 'Duplicate bloc id');
      }

      debugPrint('Registred bloc: $bloc');
      registredBlocs.add(bloc);
    }
  }

  @override
  void onError(BlocBase<dynamic> bloc, Object error, StackTrace stackTrace) {
    logger.logError(error, stackTrace, current);
  }

  @override
  void onEvent(Bloc<dynamic, dynamic> bloc, Object? event) {}

  @override
  void onTransition(Bloc<dynamic, dynamic> bloc, Transition<dynamic, dynamic> transition) {}

  String get current => (<Object>[
    appState.apiRequest,
    appState.session
  ] + registredBlocs).map((Object e) => e.toString()).join('\n');
}
