import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../exception/exceptions.dart';
import '../../reposotories/session_repositories.dart';
import '../../reposotories/user_reposotories.dart';

part 'sessions_event.dart';
part 'sessions_state.dart';

class SessionsBloc extends Bloc<SessionsEvent, SessionsState> {
  final SessionRepositories sessionRepositories;
  final UserReposotories userReposotories;
  final FirebaseAuth firebaseAuth;

  SessionsBloc({
    required this.sessionRepositories,
    required this.userReposotories,
    required this.firebaseAuth,
  }) : super(SessionUnknown()) {
    on<LoadSession>(_loadSession);
  }

  Future<void> _loadSession(SessionsEvent event, Emitter<SessionsState> emit) async {
    try{
      emit(SessionLoading());
      final String? accessToken = await sessionRepositories.getAccessToken();
      final String? refreshToken = await sessionRepositories.getRefreshToken();

      if (accessToken == null || refreshToken == null){
        await firebaseAuth.signInAnonymously();
        emit(SessionAbsent());
      }
    }on SessionExpired {
      emit(SessionLoadError());
      await sessionRepositories.deleteCredentials();
    }on NetworkException{
      emit(SessionAbsent());
    }
  }
}
