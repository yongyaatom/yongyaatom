part of 'sessions_bloc.dart';

abstract class SessionsEvent extends Equatable {
  const SessionsEvent();

  @override
  List<Object> get props => <Object>[];
}

class LoadSession extends SessionsEvent{
  const LoadSession();
}
