part of 'sessions_bloc.dart';

abstract class SessionsState extends Equatable {
  const SessionsState();
  
  @override
  List<Object> get props => <Object>[];
}

class SessionUnknown extends SessionsState {}

class SessionLoading extends SessionsState {}

class SessionLoadError extends SessionsState {}

class SessionAbsent extends SessionsState {}

class SessionPresent extends SessionsState {
  final int userId;
  const SessionPresent(this.userId);

  @override
  List<Object> get props => <Object>[userId];
}
