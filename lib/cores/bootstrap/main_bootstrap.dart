import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api/api_request.dart';
import '../blocs/base_bloc.dart';
import '../blocs/sessions/sessions_bloc.dart';
import '../dependency_injection/app_state.dart';
import '../helpers/router/app_routers.dart';
import '../keys/keys.dart';
import '../logger/logger.dart';
import '../reposotories/check_internet_connection.dart';
import '../reposotories/disponse_subscription.dart';
import '../reposotories/session_repositories.dart';
import '../reposotories/user_authentication_repositories.dart';
import '../reposotories/user_reposotories.dart';

Future<AppState> mainBootstrap() async {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final Connectivity connectivity = Connectivity();

  final Logger logger = Logger();
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  final String? initialAccessToken = await secureStorage.read(key: Keys.accessToken);
  final String? initialRefreshToken = await secureStorage.read(key: Keys.refreshToken);

  final DisponseSubscription disposeSubscription = DisponseSubscription();
  final CheckInternetConnection checkInternetConnection = CheckInternetConnection(
    connectivity: connectivity,
    disponseSubscription: disposeSubscription,
  );

  final AppRouters appRouters = AppRouters();

  final SessionRepositories sessionRepositories = SessionRepositories(
    initialAccessToken,
    initialRefreshToken,
    secureStorage,
  );

  final ApiRequest apiRequest = ApiRequest(
    host: '127.0.0.1',
    initialAccessToken: initialAccessToken,
    getAccessToken: sessionRepositories.getAccessToken,
    getRefreshToken: sessionRepositories.getRefreshToken,
    setAccessToken: sessionRepositories.setAccessToken,
  );

  final UserReposotories userReposotories = UserReposotories(apiRequest: apiRequest);

  final SessionsBloc sessionsBloc = SessionsBloc(
    sessionRepositories: sessionRepositories, 
    userReposotories: userReposotories,
    firebaseAuth: firebaseAuth,
  );

  final UserAuthenticationRepositoires userAuthentication = UserAuthenticationRepositoires(
    firebaseAuth: firebaseAuth,
    sessionRepositories: sessionRepositories,
  );

  final AppState appState = AppState(
    apiRequest: apiRequest,
    session: sessionRepositories,
    sessionsBloc: sessionsBloc,
    userAuthentication: userAuthentication,
    checkInternetConnection: checkInternetConnection,
    disposeSubscription: disposeSubscription,
    appRouters: appRouters,
  );

  final AppBlocObserver blocObserver = AppBlocObserver(
    appState,
    logger,
  );

  FlutterError.onError = (FlutterErrorDetails errorDetails){
    logger.logFlutterError(errorDetails, blocObserver.current);
  };

  return appState;
}
