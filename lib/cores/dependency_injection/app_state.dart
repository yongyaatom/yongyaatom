import 'package:flutter/widgets.dart';

import '../api/api_request.dart';
import '../blocs/sessions/sessions_bloc.dart';
import '../helpers/router/app_routers.dart';
import '../reposotories/check_internet_connection.dart';
import '../reposotories/disponse_subscription.dart';
import '../reposotories/session_repositories.dart';
import '../reposotories/user_authentication_repositories.dart';

class AppState{
  final ApiRequest apiRequest;
  final SessionRepositories session;
  final SessionsBloc sessionsBloc;
  final UserAuthenticationRepositoires userAuthentication;
  final CheckInternetConnection checkInternetConnection;
  final DisponseSubscription disposeSubscription;
  final AppRouters appRouters;

  AppState({
    required this.apiRequest,
    required this.session, 
    required this.sessionsBloc,
    required this.userAuthentication,
    required this.checkInternetConnection,
    required this.disposeSubscription,
    required this.appRouters,
  }){
    checkInternetConnection.checkInternetConnection();
    sessionsBloc.add(const LoadSession());
    userAuthentication.userAuthStateChanges();
  }

  static AppState of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppStateProvider>()!.appState;
  }
}

class AppStateProvider extends InheritedWidget{
  final AppState appState;
  const AppStateProvider({
    super.key,
    required super.child,
    required this.appState, 
  });

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => false;
}
