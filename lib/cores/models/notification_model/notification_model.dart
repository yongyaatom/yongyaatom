import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notification_model.g.dart';

abstract class NotificationData implements Built<NotificationData, NotificationDataBuilder>{
  int? get id;
  String? get name;
  int? get age;
  String? get gender;

  factory NotificationData([Function(NotificationDataBuilder b) updates]) = _$NotificationData;
  NotificationData._();
  static Serializer<NotificationData> get serializer => _$notificationDataSerializer;
}
