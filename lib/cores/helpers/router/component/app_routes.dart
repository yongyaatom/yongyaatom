enum AppRoute{
  home,
  experience,
  studies,
  about,
  contact,
  login,
  signup,
  errorPage,
}

extension AppRouteExtension on AppRoute{
  String get path {
    switch(this){
      case AppRoute.home:
        return '/';
      case AppRoute.experience:
        return '/experience';
      case AppRoute.studies:
        return '/studies';
      case AppRoute.about:
        return '/about';
      case AppRoute.contact:
        return '/contact';
      case AppRoute.login:
        return '/login';
      case AppRoute.signup:
        return '/signup';
      case AppRoute.errorPage:
        return '/errorPage';
    }
  }

  String get name {
    switch (this){
      case AppRoute.home:
        return '/';
      case AppRoute.experience:
        return 'experience';
      case AppRoute.studies:
        return 'studies';
      case AppRoute.about:
        return 'about';
      case AppRoute.contact:
        return 'contact';
      case AppRoute.login:
        return 'login';
      case AppRoute.signup:
        return 'signup';
      case AppRoute.errorPage:
        return 'errorPage';
    }
  }
}
