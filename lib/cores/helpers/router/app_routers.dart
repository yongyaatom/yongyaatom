import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../features/home/presentation/home.dart';
import '../../../features/no_internet/presentation/error_page.dart';
import '../../../features/signin/presentations/pages/signin_form.dart';
import '../../../features/signup/presentations/pages/signup_form.dart';
import 'component/app_routes.dart';


class AppRouters{
  
  GoRouter get appRoutes => _appRouters;

  final GoRouter _appRouters = GoRouter(
    
    routes: <RouteBase>[
      GoRoute(
        path: AppRoute.home.path,
        name: AppRoute.home.name,
        builder: (BuildContext context, GoRouterState state) => const HomePage(),  
      ),
      GoRoute(
        path: AppRoute.signup.path,
        name: AppRoute.signup.name,
        builder: (BuildContext context, GoRouterState state) => const SignupForm()
      ),
      GoRoute(
        path: AppRoute.login.path,
        name: AppRoute.login.name,
        builder: (BuildContext context, GoRouterState state) => const SigninForm(),
      ),
      GoRoute(
        path: AppRoute.errorPage.path,
        name: AppRoute.errorPage.name,
        builder:(BuildContext context, GoRouterState state) => const ErrorPage(),
      )
    ],
    errorBuilder: (BuildContext context, GoRouterState state) {
      return const MaterialApp(
        home: ErrorPage(),
      );
    },
  );
}
