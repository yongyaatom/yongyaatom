import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

class DisponseSubscription{
  late StreamSubscription<ConnectivityResult> internetStreamSubscription;

  Future<void> disposeSubscription() async {
    try{
      await internetStreamSubscription.cancel();
    }catch (e){
      rethrow;
    }
  }
  

}
