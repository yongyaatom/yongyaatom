import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../exception/exceptions.dart';
import '../exception/user_authentication_exceptions.dart';
import 'session_repositories.dart';

part 'user_state.dart';

class UserAuthenticationRepositoires{
  final FirebaseAuth firebaseAuth;
  final SessionRepositories sessionRepositories;
  UserAuthenticationRepositoires( {
    required this.firebaseAuth,
    required this.sessionRepositories,
  });

  // https://bloclibrary.dev/#/flutterfirebaselogintutorial?id=repository
  // https://firebase.flutter.dev/docs/auth/usage/#authentication-state
  Future<void> userAuthStateChanges() async {
    try{
      firebaseAuth.authStateChanges().listen((User? user) {
        if (user == null || user.isAnonymous){
          return;
        }else{
          log('User is Signin');
        }
      });
    }on FirebaseAuthException catch(e){
      // https://firebase.google.com/docs/auth/flutter/errors
      throw AuthenticationException(e.code);
    }on NetworkException catch(e){
      throw NetworkException(message: e.userMessage);
    }
  }

  

  

  

}
