import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../exception/exceptions.dart';
import '../keys/keys.dart';

class SessionRepositories extends Cubit<bool>{
  String? _accessToken;
  String? _refreshToken;

  FlutterSecureStorage secureStorage;
  bool get isLoggedIn => _accessToken != null && _refreshToken != null;

  SessionRepositories(
    this._accessToken,
    this._refreshToken,
    this.secureStorage,
  ): super(_accessToken != null && _refreshToken != null);

  Future<void> setCredentials(String accessToken, String refreshToken) async {
    await secureStorage.write(key: Keys.accessToken, value: accessToken);
    await secureStorage.write(key: Keys.refreshToken, value: refreshToken);
    _accessToken = accessToken;
    _refreshToken = refreshToken;
    emit(isLoggedIn);
  }

  Future<void> deleteCredentials() async {
    await secureStorage.delete(key: Keys.accessToken);  
    await secureStorage.delete(key: Keys.refreshToken);
    _accessToken = null;
    _refreshToken = null;
    emit(isLoggedIn);
  }

  Future<String?> getAccessToken() async {
    return await secureStorage.read(key: Keys.accessToken);
  }

  Future<String?> getRefreshToken() async {
    return await secureStorage.read(key: Keys.refreshToken);
  }

  Future<void> setAccessToken(String accessToken) async {
    final bool refreshToken = await secureStorage.containsKey(key: Keys.refreshToken);

    if(!refreshToken){
      throw BadStateException('Required refresh token when updating access token.', 'Failed to authenticate.');
    }

    await secureStorage.write(key: Keys.accessToken, value: accessToken);
  }
}
