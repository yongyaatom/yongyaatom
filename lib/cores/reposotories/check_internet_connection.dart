import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'disponse_subscription.dart';

class CheckInternetConnection extends Cubit<bool>{
  final Connectivity connectivity;
  final DisponseSubscription disponseSubscription;

  CheckInternetConnection({
    required this.connectivity,
    required this.disponseSubscription,
  }): super(false);

  late bool _isInternetConnected = false;
  bool get isInternetConnected => _isInternetConnected;

  Future<void> checkInternetConnection ()  async {
    try{
        disponseSubscription.internetStreamSubscription = connectivity.onConnectivityChanged.listen(
        (ConnectivityResult internetStatus) async {
          if (internetStatus == ConnectivityResult.none){
            _isInternetConnected = false;
            emit(isInternetConnected); 
          }else{
            _isInternetConnected = true;
            emit(isInternetConnected);
          }
      });
      // also implement timeout for long time connection error
    }on SocketException catch (e){
      throw SocketException(e.message);
    }on PlatformException catch (e){
      throw PlatformException(code: e.code);
    }
  }
}
