import '../api/api_request.dart';
import '../models/users/users.dart';

class UserReposotories{
  final ApiRequest apiRequest;

  UserReposotories({required this.apiRequest});

  Future<UserModel> getUser() async {
    return await apiRequest.get<UserModel>(path: 'auth/user');
  }
}
