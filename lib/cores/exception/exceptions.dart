abstract class BaseException implements Exception{
  final String userMessage;
  final String debugMessage;

  BaseException(
    this.userMessage,
    this.debugMessage,
  );
}

class BadStateException extends BaseException{
  BadStateException(super.userMessage, super.debugMessage);
}

class NetworkException extends BaseException{
  NetworkException({
    String message = 'Failed to connect'
  }): super('Network error', message);
}

abstract class ApiError implements BaseException{
  final int code;
  final String uri;
  @override
  final String debugMessage;
  @override
  final String userMessage;

  ApiError(this.code, this.uri, this.debugMessage, this.userMessage);
}

class SessionExpired extends ApiError{
  SessionExpired(Uri url, String message) : super(401, url.toString(), 'Session Expired', message);
}

class BadRequest extends ApiError{
  BadRequest(Uri url, String message) : super(400, url.toString(), 'Bad Request', message);
}

class ServerError extends ApiError{
  ServerError(Uri url, String message) : super(500, url.toString(), 'Server Error', message);
}

class NotFound extends ApiError{
  NotFound(Uri url, String message) : super(404, url.toString(), 'Not Found', message);
}

class UnKnownError extends ApiError{
  UnKnownError(Uri url, String message) : super(520, url.toString(), 'Unknown error', message);
}
