class AuthenticationException implements Exception{
  final String message;
  AuthenticationException([this.message = 'Something is wrong']);

  factory AuthenticationException.fromCode(String code) {
    switch(code){
      case 'invalid-email':
         return AuthenticationException(
          'Email is not valid'
         );
      default:
        return AuthenticationException();
    }
    
  }
}
